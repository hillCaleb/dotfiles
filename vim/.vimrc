"  ____      _      _     _      __     ___             ____   ____ 
" / ___|__ _| | ___| |__ ( )___  \ \   / (_)_ __ ___   |  _ \ / ___|
"| |   / _` | |/ _ \ '_ \|// __|  \ \ / /| | '_ ` _ \  | |_) | |    
"| |__| (_| | |  __/ |_) | \__ \   \ V / | | | | | | | |  _ <| |___ 
" \____\__,_|_|\___|_.__/  |___/    \_/  |_|_| |_| |_| |_| \_\\____|
"                                                                   
"
"set t_Co=256
set number relativenumber
syntax on
set smartindent
set shiftwidth=4
set tabstop=4
set softtabstop=0
set expandtab
set smarttab
set foldmethod=syntax
let g:rustfmt_autosave = 1
autocmd vimenter * NERDTree

filetype plugin indent on

map <C-j> <C-W>j
map <C-k> <C-W>k
map <C-h> <C-W>h
map <C-l> <C-W>l

if (&tildeop)
  nmap gcw guw~l
  nmap gcW guW~l
  nmap gciw guiw~l
  nmap gciW guiW~l
  nmap gcis guis~l
  nmap gc$ gu$~l
  nmap gcgc guu~l
  nmap gcc guu~l
  vmap gc gu~l
else
  nmap gcw guw~h
  nmap gcW guW~h
  nmap gciw guiw~h
  nmap gciW guiW~h
  nmap gcis guis~h
  nmap gc$ gu$~h
  nmap gcgc guu~h
  nmap gcc guu~h
  vmap gc gu~h
endif
" functions
function! s:check_back_space() abort
  let col = col('.') - 1
  return !col || getline('.')[col - 1]  =~ '\s'
endfunction

inoremap <silent><expr> <TAB>
      \ pumvisible() ? "\<C-n>" :
      \ <SID>check_back_space() ? "\<TAB>" :
      \ coc#refresh()

inoremap " ""<left>
inoremap ' ''<left>
inoremap ( ()<left>
inoremap [ []<left>
inoremap { {}<left>
inoremap {<CR> {<CR>}<ESC>O
inoremap {;<CR> {<CR>};<ESC>O
" xml formating stuff
function! DoPrettyXML()
  " save the filetype so we can restore it later
  let l:origft = &ft
  set ft=
  " delete the xml header if it exists. This will
  " permit us to surround the document with fake tags
  " without creating invalid xml.
  1s/<?xml .*?>//e
  " insert fake tags around the entire document.
  " This will permit us to pretty-format excerpts of
  " XML that may contain multiple top-level elements.
  0put ='<PrettyXML>'
  $put ='</PrettyXML>'
  silent %!xmllint --format -
  " xmllint will insert an <?xml?> header. it's easy enough to delete
  " if you don't want it.
  " delete the fake tags
  2d
  $d
  " restore the 'normal' indentation, which is one extra level
  " too deep due to the extra tags we wrapped around the document.
  silent %<
  " back to home
  1
  " restore the filetype
  exe "set ft=" . l:origft
endfunction
command! PrettyXML call DoPrettyXML()


" Get the defaults that most users want.
"source $VIMRUNTIME/defaults.vim


if &t_Co > 2 || has("gui_running")
  " Switch on highlighting the last used search pattern.
  set hlsearch
endif

" Put these in an autocmd group, so that we can delete them easily.
augroup vimrcEx
  au!

  " For all text files set 'textwidth' to 78 characters.
  autocmd FileType text setlocal textwidth=78
augroup END

if has('syntax') && has('eval')
  packadd! matchit
endif
set termguicolors
if empty(glob('~/.vim/autoload/plug.vim'))
  silent !curl -fLo ~/.vim/autoload/plug.vim --create-dirs
    \ https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
  autocmd VimEnter * PlugInstall --sync | source $MYVIMRC
endif

call plug#begin('~/.vim/plugged')
if has('nvim')
    Plug 'neoclide/coc.nvim', {'branch': 'release'}
    Plug 'rust-lang/rust.vim'
    Plug 'vim-airline/vim-airline'
    Plug 'vim-airline/vim-airline-themes'
    Plug 'morhetz/gruvbox'
	Plug 'preservim/nerdtree'
    Plug 'prettier/vim-prettier', { 'do': 'yarn install' }
    Plug 'tpope/vim-fugitive'
else 
    Plug 'neoclide/coc.nvim', {'branch': 'release'}
    Plug 'roxma/nvim-yarp'
    Plug 'roxma/vim-hug-neovim-rpc'
    Plug 'Shougo/neco-syntax'
    Plug 'vim-scripts/DrawIt'
    Plug 'vim-airline/vim-airline'
    Plug 'vim-airline/vim-airline-themes'
    Plug 'morhetz/gruvbox'
	Plug 'preservim/nerdtree'
	Plug 'lervag/vimtex'
endif

let g:LanguageClient_serverCommands = {
\ 'rust': ['rust-analyzer'],
\ }

call plug#end()
set undodir=~/vimtmp/.undo/
set backupdir=~/vimtmp/.backup/
set directory=~/vimtmp/.swp/
set background=dark
color gruvbox
